import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Notification } from '../utils/notification';
import { FormControl, FormGroup, Validators} from '@angular/forms';
import * as $ from 'jquery';
import { isString } from 'util';
import { SiteRepository } from '../repository/site-repository';
import { DatabaseService } from '../service/database.service';
import { Site } from '../model/site';

@Component({
  selector: 'app-addsite',
  templateUrl: './addsite.component.html',
  styleUrls: ['./addsite.component.scss']
})
export class AddsiteComponent implements OnInit {

  isTestBtnVisible : boolean;
  isSaveBtnVisible : boolean;
  isLoadingVisible : boolean;

  ssl : string;
  domainName : string;
  siteUsername : string;
  sitePassword: string;

  constructor( 
    private http: HttpClient,
    private db: DatabaseService
  ) { }

  ngOnInit() 
  {
    this.isSaveBtnVisible = true;
    this.isTestBtnVisible = true;
    this.isLoadingVisible = false;
    /**
     * preselect https://
     */
    this.ssl = '1';
  }

  public addSite()
  {
    if( ! this.validate() )
    {
      return;
    }
    this.toggleLoader();
    let testResult: any;
    let httpOptions = {
      headers:  new HttpHeaders({
        'Content-Type':  'application/x-www-form-urlencoded'
      })
    }
    let response = this.http.post(
      this.getUrlPath(),
      $.param({
        name: this.siteUsername, 
        pass: this.sitePassword
      }),
      httpOptions
    );
    response
    .subscribe( 
      ( data ) =>  this.saveSite( data ),
      ( errorData ) => this.testError( errorData )
    );
  }

  private saveSite ( data )
  {
    /**
     * check if data contains params
     */
    this.toggleLoader();
    let notification : Notification = new Notification;
    console.log( JSON.stringify(data.data[0].JWT ) );
    if(  typeof data.data[0].jwt === 'string' && 
      data.data[0].jwt.includes('.') )
    {
      let siteRepository : SiteRepository = new SiteRepository( this.db );
      let site : Site = siteRepository.create( data.data[0].jwt, this.siteUsername, this.sitePassword, this.domainName, this.ssl  );
      siteRepository.insert( 
        site , 
        (tx, result) => {
          notification.setBody( 'Site added' );
          notification.displaySuccess();
        }, (tx, err) => {
          notification.setBody( 'Database error when adding site' );
          notification.displayError();
        });
    }
    else
    {
      notification.setBody( 'Server response does not contain authorization token' );
      notification.displayError();
    }
  }

  private toggleLoader() : void
  {
    if( this.isLoadingVisible )
    {
      this.isSaveBtnVisible = true;
      this.isTestBtnVisible = true;
      this.isLoadingVisible = false;
    }
    else
    {
      this.isSaveBtnVisible = false;
      this.isTestBtnVisible = false;
      this.isLoadingVisible = true;
    }
  }

  public testConnection() : void
  {
    if( ! this.validate() )
    {
      return;
    }
    this.toggleLoader();
    let testResult: any;
    let httpOptions = {
      headers:  new HttpHeaders({
        'Content-Type':  'application/x-www-form-urlencoded'
      })
    }
    let response = this.http.post(
      this.getUrlPath(),
      $.param({'name': this.siteUsername, 'pass': this.sitePassword}),
      httpOptions
    );
    response
    .subscribe( 
      ( data ) =>  this.testSuccess( data ),
      ( errorData ) => this.testError( errorData )
    );
  }

  private testSuccess( data ) : void
  {
    this.toggleLoader();
    console.log( JSON.stringify( data ) );
    let notification : Notification = new Notification;
    notification.setBody( 'Connection successful\nClick Save and start monitoring site' );
    notification.displaySuccess();
  }

  private testError( errorData ) : void
  {
    this.toggleLoader();
    let notification : Notification = new Notification;
    if( errorData.error.errors instanceof Array )
    {
      errorData.error.errors.forEach(element => {
        console.log( JSON.stringify( element ))
        if( isString ( element.message ) )
        {
          notification.append( '\n' + element.message );
        }
      });
    }
    else
    {
      notification.setBody( 'Please check if:\n-Drupamonitor module is enabled on your site\n-Http protocol is correct\n-Domain is correct\n-If your device has internet connection' );
    }
    notification.displayError();
  }

  private validate() : boolean
  {
    let isValid : boolean = true;
    let notification : Notification = new Notification;
    if( ! this.domainName )
    {
      isValid = false;
      notification.append( '\n' + 'Domain name is required' );
    }
    else if( this.domainName.includes( ' ' ) )
    {
      isValid = false;
      notification.append( '\n' + 'Domain name cannot contain spaces' );
    }
    if( ! this.siteUsername )
    {
      isValid = false;
      notification.append( '\n' + 'Username is required' );
    }
    if( ! this.sitePassword )
    {
      isValid = false;
      notification.append( '\n' + 'Password is required' );
    }
    if( ! isValid )
    {
      notification.displayError();
      return false;
    }
    return true;
  }

  private getUrlPath()
  {
    let urlPrefix : string;
    if(  this.ssl === '1')
    {
      urlPrefix =  'https://';
    }
    else
    {
      urlPrefix = 'http://';
    } 
    return urlPrefix + this.domainName + '/drupamonitor/api/test/connection';
  }
}
 