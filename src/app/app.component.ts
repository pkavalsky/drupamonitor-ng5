import { Component,   OnInit  } from '@angular/core';
import { Routes, RouterModule, Router, UrlTree } from '@angular/router';
import { DatabaseService } from './service/database.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  implements OnInit 
{
  title = 'Drupamonitor';
  
  constructor ( private router : Router,
                private databaseService: DatabaseService ) {
  }
  
  ngOnInit() 
  {
    
  }

  hidebsNav()
  {
    $('.navbar-toggler').trigger( "click" );
  }

  getMenuItemActive(routeName : string) : string
  {
      if( this.router.isActive( routeName, true ) )
      {
        return 'active';
      }
      return '';
  }
  
}



