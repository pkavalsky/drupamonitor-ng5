import { Component, OnInit } from '@angular/core';
import { UserService } from '../service/user.service';
import { SiteRepository } from '../repository/site-repository';
import { Site } from '../model/site';
import { DatabaseService } from '../service/database.service';
import { Notification } from '../utils/notification';
import * as $ from 'jquery';
import { ViewChild, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit 
{
  public sites: Array<Site>;
  public dummyproperty: string;

  constructor( 
    private userService: UserService, 
    private databaseService: DatabaseService ) 
  {
      
  }

  ngOnInit()
  {
    this.initializeHome();
  }

  public initializeHome()
  {
    /*************************
     * do not remove this !  *
     * without assigning ANY * 
     * value to ANY property * 
     * callback properties   * 
     * will not be populated * 
     * on DOM during initial * 
     * load                  *
     *************************/
    this.dummyproperty = '';
    this.databaseService.createDatabaseStructure( (tx, result) => {
      /**
       * create database success callback
       */
      let siteRepository : SiteRepository = new SiteRepository( this.databaseService ) ;
      siteRepository.findAll( ( sites: Array<Site>) => {
        /**
         * finding sites success callback
         */
        this.sites = sites;
      }, (tx, error) => {
        /**
         * finding sites error callback
         */
        (new Notification).setBody( 'Error getting sites data' ).displayError();
      });
    }, (tx, err) => {
      /**
       * create database error callback
       */
      (new Notification).setBody( 'Error checking sites data' ).displayError();
    });
  }
}
