export class Site 
{
    private JWT : string;
    private username : string;
    private password : string;
    private domain : string
    private ssl : boolean;
    private id : number;

    public constructor( JWT:string, username:string, password:string, domain:string , ssl:boolean  )
    {
        this.JWT = JWT;
        this.username = username;
        this.password = password;
        this.domain = domain;
        this.ssl = ssl;
    }

    public setId( id )
    {
        this.id = id;
    }

    public setJwt( JWT )
    {
        this.JWT = JWT;
    }

    public setUsername( username )
    {
        this.username = username;
    }

    public setPassword( password )
    {
        this.password = password;
    }

    public setDomain( domain )
    {
        this.domain = domain;
    }

    public setSsl( ssl )
    {
        this.ssl = ssl;
    }

    public getJwt()
    {
        return this.JWT;
    }

    public getUsername()
    {
        return this.username;
    }

    public getPassword()
    {
        return this.password;
    }

    public getDomain()
    {
        return this.domain;
    }

    public getSsl()
    {
        return this.ssl;
    }
}
