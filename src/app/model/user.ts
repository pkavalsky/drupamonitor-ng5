export class User 
{
    public getNotificationMethod()
    {
        var notificationMethod : string;
        notificationMethod =  localStorage.getItem( 'notificationMethod');
        if ( notificationMethod == null )
        {
            return 'vibration';
        }
        return notificationMethod;
    }

    public getNotificationLogLevel()
    {
        var notificationLogLevel : string;
        notificationLogLevel =  localStorage.getItem( 'notificationLogLevel');
        if( notificationLogLevel == null )
        {
            return '0';
        }
        return notificationLogLevel;
    }

    public getLogsRefreshFrequency()
    {
       var logsRefreshFrequency : string;
       logsRefreshFrequency =  localStorage.getItem( 'logsRefreshFrequency');
        if( logsRefreshFrequency == null )
        {
            return '5';
        }
        return logsRefreshFrequency;
    }

    public setNotificationMethod( notificationMethod )
    {
        localStorage.setItem( 'notificationMethod', notificationMethod  );
    }

    public setNotificationLogLevel( notificationLogLevel )
    {
        localStorage.setItem( 'notificationLogLevel', notificationLogLevel  );
    }

    public setLogsRefreshFrequency( logsRefreshFrequency )
    {
        localStorage.setItem( 'logsRefreshFrequency', logsRefreshFrequency  );
    }
}
