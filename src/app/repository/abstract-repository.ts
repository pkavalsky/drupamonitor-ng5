import { DatabaseService } from '../service/database.service';

export abstract class AbstractRepository 
{
    public constructor( protected databaseService : DatabaseService ) 
    {
        
    }

    protected logDbError( err )
    {
        console.log(' =========DB ERROR START =======');
        console.log ( err.code );
        console.log ( err.message );
        console.log(' =========DB ERROR END =======');
    }
}
