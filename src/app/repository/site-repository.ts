import { DatabaseService } from '../service/database.service';
import { Site } from '../model/site';
import { User } from '../model/user';
import { AbstractRepository } from './abstract-repository';

export class SiteRepository extends AbstractRepository
{
    public create ( JWT:string, username:string, password:string, domainName:string , ssl:string ) : Site
    {
        let boolSsl
        if( ssl === '0' )
        {
            boolSsl = false;
        }
        else
        {
            boolSsl = true;
        }
        return new Site (  JWT, username, password, domainName , boolSsl  );
    }

    public insert ( site: Site , successCallback: (tx, results) => void, errorCallback : (tx, error) => void  )
    {
        this.databaseService.getDatabase().transaction( 
        (tx) => {
            tx.executeSql("INSERT INTO site ( jwt, username, password, domain, ssl ) VALUES (?, ?, ?, ?, ?)", [
                site.getJwt(), site.getUsername(), site.getPassword(), site.getDomain(), site.getSsl()
            ], successCallback, 
            (tx, error) => {
                this.logDbError( tx );
                errorCallback( tx, error );
            });
        }, (tx, err) => {
            this.logDbError( tx );
            errorCallback( tx, err );
        });
    }

    public findAll( successCallback: ( sites : Array<Site> ) => void, errorCallback : (tx, error) => void  )
    {
        this.databaseService.getDatabase().transaction( 
            (tx) => {
                tx.executeSql("SELECT * FROM site;", [], (tx, results) => {
                    let length : any = results.rows.length;
                    let sites: Array<Site> = new Array;
                    for( let i = 0; i < length; i ++)
                    {
                        let row: any = results.rows[i];
                        let site: Site = new Site( row.jwt, row.username, row.password, row.domain, row.ssl );
                        sites.push( site );
                    }
                    successCallback( sites );
                }, (tx, error) => {
                    this.logDbError( tx );
                    errorCallback( tx, error );
                });
            } , (tx, error) => {
                this.logDbError( tx );
                errorCallback( tx, error );
            }
        );
    }

    public find( id:number )
    {
        
    }
}
