import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {

  private database: any;

  constructor() 
  {
    this.database = window.openDatabase("Database", "1.0", "Drupamonitor", 200000);
  }

  getDatabase()
  {
      return this.database;
  }

  createDatabaseStructure( successCallback: (tx, results) => void, errorCallback : (tx, error) => void )
  {
    this.database.transaction( 
      /**
       * transaction
       */
      (tx) => 
      {
        tx.executeSql('CREATE TABLE IF NOT EXISTS site (id INTEGER PRIMARY KEY AUTOINCREMENT, jwt TEXT, username TEXT, password TEXT, domain TEXT, ssl BOOLEAN )');
      } , 
      /**
       * error callback
       */
      (tx, err) => 
      {
        errorCallback( tx, err );
        console.log(' =========DB ERROR START =======');
        console.log ( tx.code );
        console.log ( tx.message );
        console.log(' =========DB ERROR END =======');
      },
      /**
       * success callback
       */
      (tx, result) => 
      {
        successCallback (tx, result);
      });
  }
}
