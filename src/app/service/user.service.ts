import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { User } from '../model/user';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class UserService 
{
  private user  = new User;


  constructor() 
  {
    
  }

  getUser()
  {
    return this.user;
  }
}
