import { Component, OnInit } from '@angular/core';
import { UserService } from '../service/user.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { User } from '../model/user';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})

export class SettingsComponent implements OnInit {

  private logLevel:string = "";
  private notificationMethod:string = "";
  private logLevelFrequency:string = "";
  private user:User;

  constructor( 
    private userService: UserService,
    ) 
  {
    
  }

  ngOnInit() 
  {
    /**
     * prefill properties
     */
    this.user = this.userService.getUser();
    this.logLevel = this.user.getNotificationLogLevel();
    this.notificationMethod = this.user.getNotificationMethod();
    this.logLevelFrequency = this.user.getLogsRefreshFrequency();
  }

  updateLogLevel(loglevel)
  {
    this.user.setNotificationLogLevel(loglevel);
  }

  updateNotificationMethod(notificationMethod)
  {
    this.user.setNotificationMethod( notificationMethod );
  }

  updateLogLevelFrequency( logLevelFrequency )
  {
    this.user.setLogsRefreshFrequency( logLevelFrequency );
  }

}
