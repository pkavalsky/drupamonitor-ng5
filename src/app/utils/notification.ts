
export class Notification 
{
    private title : string;
    private body : string;
    private btn : string;

    constructor()
    {

    }


    public setTitle( title: string)
    {
        this.title = title;
        return this;
    }

    public setBody( body: string )
    {
        this.body = body;
        return this;
    }

    public setBtn( btn: string)
    {
        this.btn = btn;
        return this;
    }

    public display()
    {
        navigator.notification.alert(
            this.body,
            () => {},
            this.title,
            this.btn
        );
        return this;
    }

    public displayError()
    {
        navigator.notification.alert(
            this.body,
            () => {},
            'Error',
            'Ok'
        );
        return this;
    }

    public displaySuccess()
    {
        navigator.notification.alert(
            this.body,
            () => {},
            'Success',
            'Ok'
        );
        return this;
    }

    public append( body : string )
    {
        if(  typeof this.body === 'undefined' )
        {
            this.body = '';
        }
        this.body += body;
    }
}
